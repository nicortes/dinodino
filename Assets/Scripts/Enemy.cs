﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject explosionParticles;
    [SerializeField] Transform parent;
    [SerializeField] int scorePerHit = 12;
    [SerializeField] int hitsToKill = 10;

    ScoreBoard scoreBoard;

    void Start() {
        /* 
         * If a Standard Asset gets an update, the components put by the developer will be lost. 
         * This is an exercise to add components to game objects at runtime.
        */
        AddAudioSourceComponent();

        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void AddAudioSourceComponent() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 0.1f;
    }

    private void OnParticleCollision(GameObject other) {
        ProcessHit();
        if (hitsToKill < 1) {
            KillEnemy();
        }
    }

    private void ProcessHit() {
        scoreBoard.ScoreHit(scorePerHit);
        hitsToKill--;
    }

    private void KillEnemy() {
        // +20 or -20 offset to center explosion is hard coded. This could be improved.
        Vector3 position;
        if (transform.rotation.y > 0) {
            position = new Vector3(transform.position.x - 20, transform.position.y, transform.position.z);
        } else {
            position = new Vector3(transform.position.x + 20, transform.position.y, transform.position.z);
        }

        GameObject deathFX = Instantiate(explosionParticles, position, Quaternion.identity);

        deathFX.transform.parent = parent;// Putting the particles under a gameobject, so the scene doesn't get too crowded (there's also a SelfDestruct script attached to the explosion particle prefab)

        Destroy(gameObject);
    }
}
