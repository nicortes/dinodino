﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    [Header("General")]
    [Tooltip("In ms^1")] [SerializeField] float xSpeed = 15f;
    [Tooltip("In ms^1")] [SerializeField] float ySpeed = 10f;
    [Tooltip("In meters")] [SerializeField] float xRange = 5f;
    [Tooltip("In meters")] [SerializeField] float yRange = 4f;
    [SerializeField] GameObject beam;

    // Pitch (movement that DinoDino makes when going up or down)
    // Yaw (movement that DinoDino makes when going left or right)
    // Roll (movement that DinoDino makes also when going left or right)

    [Header("Screen-position Based")]
    [Tooltip("How much DinoDino rotates in the X axis so it's always facing the camera")]
    [SerializeField] float positionPitchFactor = -5f;
    [Tooltip("How much DinoDino rotates in the Y axis so it's always facing the camera")]
    [SerializeField] float positionYawFactor = 4f;

    [Header("Control-throw Based")]
    [Tooltip("How pronounced the pitch is (the rotation in the X axis when moving up or down)")]
    [SerializeField] float controlPitchFactor = -20f;
    [Tooltip("How pronounced the roll is (the rotation in the Z axis when moving left or right)")]
    [SerializeField] float controlRollFactor = -20f;
    
    float xThrow, yThrow;
    bool isControlEnabled = true;
    
    // Update is called once per frame
    void Update() {
        if (isControlEnabled) {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    // Called by string reference
    private void OnPlayerDeath() {
        isControlEnabled = false;
        //transform.localRotation = Quaternion.Euler(0, transform.localRotation.y, transform.localRotation.z);
    }

    private void ProcessRotation() {

        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControlThrow = yThrow * controlPitchFactor;
        float pitch = pitchDueToPosition + pitchDueToControlThrow;

        float yawDueToPosition = transform.localPosition.x * positionYawFactor;
        float yaw = yawDueToPosition;


        float rollDueToControlThrow = xThrow * controlRollFactor;
        float roll = rollDueToControlThrow;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessTranslation() {
        horizontalMovement();
        verticalMovement();
    }

    private void horizontalMovement() {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * xSpeed * Time.deltaTime;

        float rawXPosition = transform.localPosition.x + xOffset;
        float clampedXPosition = Mathf.Clamp(rawXPosition, -xRange, xRange);

        transform.localPosition = new Vector3(clampedXPosition, transform.localPosition.y, transform.localPosition.z);
    }

    private void verticalMovement() {
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = -yThrow * ySpeed * Time.deltaTime;

        float rawYPosition = transform.localPosition.y + yOffset;
        float clampedYPosition = Mathf.Clamp(rawYPosition, -yRange, yRange);

        transform.localPosition = new Vector3(transform.localPosition.x, clampedYPosition, transform.localPosition.z);
    }

    void ProcessFiring() {
        if (CrossPlatformInputManager.GetButton("Fire")) {
            SetBeamActive(true);
        } else {
            SetBeamActive(false);
        }
    }

    private void SetBeamActive(bool isActive) {
        /* So shot particles don't disappear after releasing the Fire button.
         * Note: This works better with Simulation Space set to World instead of Local.
         * ParticleSystem.EmissionModule emissionModule = beam.GetComponent<ParticleSystem>().emission;
         * emissionModule.enabled = isActive;
        */
        beam.SetActive(isActive);
    }
}
