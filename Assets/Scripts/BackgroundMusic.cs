﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    void Awake()
    {
        int amountOfBackgroundMusics = FindObjectsOfType<BackgroundMusic>().Length;

        if (amountOfBackgroundMusics > 1) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
