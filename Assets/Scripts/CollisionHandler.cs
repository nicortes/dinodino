﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //ok as long as this is the only script that loads scenes

public class CollisionHandler : MonoBehaviour
{
    [Tooltip("In seconds")]
    [SerializeField] float levelLoadDelay = 1f;

    [Tooltip("FX prefab on player")]
    [SerializeField] GameObject explosionGameObject;

    private void OnTriggerEnter(Collider other) {
        StartDeathSequence();
    }

    private void StartDeathSequence() {
        gameObject.SendMessage("OnPlayerDeath");
        explosionGameObject.SetActive(true);
        Invoke("RestartLevel",levelLoadDelay);
    }
    
    void RestartLevel() {// string reference
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
